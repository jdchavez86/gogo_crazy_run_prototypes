using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public GameObject menu;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        menu = GameObject.FindGameObjectWithTag("menu");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void OnCancel(InputAction.CallbackContext context)
    {
        if (context.action.triggered)
        {
            ShowOrHideMenu();
        }
    }

    public void ShowOrHideMenu()
    {
        menu.SetActive(!menu.activeSelf);
    }

   
}
