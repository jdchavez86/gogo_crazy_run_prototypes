using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEditor;



[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float playerSpeed = 2.0f;
    [SerializeField]
    private float jumpHeight = 1.0f;
    [SerializeField]
    private float gravityValue = -9.81f;

    private LevelManager levelManager;
    private CharacterController controller;
    private Vector3 playerVelocity;
    private Vector2 movementInput = Vector2.zero;
    private Material playerMaterial;
    public Image healthImage;
    public GameObject bulletSpawner;
    public GameObject bullet;
    public GameObject goal;

    private bool jumped = false;
    private bool groundedPlayer;
    private bool isPlayerBlocked;
    public bool isStuned;
    public bool isInvulnerable;
    public int playerState; // 1-lleg� a la meta, 2-muri�, 3-paralizado 
    public string playerName;
    public float health;
    public float waitTime;
    public float invulnerableTime;
    public float elpasedTimeStun;
    public float elpasedTimeOfInvulnerability;

    private void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
        controller = gameObject.GetComponent<CharacterController>();
        playerMaterial = GetComponent<Renderer>().material;
        bulletSpawner = transform.Find("bullet_spawner").gameObject;
        isInvulnerable = false;
        isPlayerBlocked = false;

        levelManager.alivePlayers++;
    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;

        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }   

        

        if (isStuned == false && health > 0f && LevelManager.theMatchHasStarted == true && isPlayerBlocked == false)
        {
            Vector3 move = new Vector3(movementInput.x, 0, 0);
            controller.Move(move * Time.deltaTime * playerSpeed);

            if (move != Vector3.zero)
            {
                gameObject.transform.forward = -move;
            }

            // Changes the height position of the player..

            if (jumped && groundedPlayer)
            {
                playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }

            playerVelocity.y += gravityValue * Time.deltaTime;
            controller.Move(playerVelocity * Time.deltaTime);
        }
        else
        {
            elpasedTimeStun += Time.deltaTime;
            if (elpasedTimeStun> waitTime)
            {
                elpasedTimeStun = 0f;
                isStuned = false;
            }
        }

       transform.position = new  Vector3(transform.position.x, transform.position.y, 0f);

        if (isInvulnerable == true)
        {
            elpasedTimeOfInvulnerability += Time.deltaTime;
            if (elpasedTimeOfInvulnerability > invulnerableTime)
            {
                elpasedTimeOfInvulnerability= 0f;
                isInvulnerable = false;
                ControlTheInvulnerability();
                Debug.Log("is invulnerable " + isInvulnerable);
            }
        }

    }

    public void OnMove(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        jumped = context.action.triggered;        
    }

    public void OnFire(InputAction.CallbackContext context)
    {

        if (context.action.triggered)
        {
            Fire();
        }
    }

    public void Fire()
    {
        if (bulletSpawner != null && isPlayerBlocked == false)
        {
            GameObject newBullet = Instantiate(bullet, bulletSpawner.transform.position, bulletSpawner.transform.rotation);
        }

        
    }

    public void TakeDamage(float damageAmount)
    {
        

        if (isInvulnerable == false && isPlayerBlocked == false)
        {
            if (health > 0)
            {
                health -= damageAmount;
                float temphealth = health / 100;
                healthImage.fillAmount = temphealth;
                Debug.Log(healthImage.fillAmount);

                //la siguiente condici�n solamente sucede si est� vivo

                if (health <= 0) {
                    levelManager.alivePlayers--;
                    levelManager.ShowResults("perdedores todos");
                }
            }

            Stun();
            isInvulnerable = true;
            ControlTheInvulnerability();
            Debug.Log("is invulnerable " + isInvulnerable);
        }

        
    }

    public void Stun()
    {
        isStuned = true;
    }

    public void ControlTheInvulnerability()
    {

        if (isInvulnerable == true)
        {
            playerMaterial.ToFadeMode();  
        }
        else
        {
            playerMaterial.ToOpaqueMode();
        }
    }

    
    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.tag == "Finish")
        //{
        //    LevelManager.somePlayerHasReachedTheGoal = true;
        //}

        if (other.gameObject == goal)
        {
            if (LevelManager.somePlayerHasReachedTheGoal == false){
                LevelManager.somePlayerHasReachedTheGoal = true;
                levelManager.ShowResults(playerName);
                isPlayerBlocked = true;
                //habr�a tambi�n que desactivarle al player su collider de ataque.
            }
            
        }

        if (other.gameObject.tag == "hurt_zone")
        {
            TakeDamage(20f);
        }
    }

   
}
