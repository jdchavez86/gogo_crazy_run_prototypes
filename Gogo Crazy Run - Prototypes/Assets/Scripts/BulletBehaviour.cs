using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{

    public float speed;
    public float bulletLifeTime;
    
    // Start is called before the first frame update
    void Start()
    {
        if (this.gameObject != null)
        {
            Destroy(this.gameObject, bulletLifeTime);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-Vector3.forward * Time.deltaTime* speed);
    }



    private void OnTriggerEnter(Collider other)
    {
        PlayerController otherPlayerController = other.gameObject.GetComponent<PlayerController>();

        if (other.gameObject.tag == "Player" && otherPlayerController.isStuned == false)
        {
            otherPlayerController.TakeDamage(20f);
            if (this.gameObject!= null)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
