using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDetector : MonoBehaviour
{

    private PlayerController playerController;
    
    // Start is called before the first frame update
    void Start()
    {
        playerController = transform.GetComponentInParent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "attack_point"){

            playerController.TakeDamage(10f);
            playerController.Stun();
        }
    }
}
