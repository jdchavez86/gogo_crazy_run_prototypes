using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Player1;
    public GameObject Player2;
    public GameObject goal;
    public GameObject levelMenu;
    public bool finishedLevel = false;
    public static bool theMatchHasStarted;
    public static bool somePlayerHasReachedTheGoal;
    public TextMeshProUGUI textFeedback;
    public int alivePlayers;
    private int remainingSecondsToStart;
    private float elapsedTimeToStart;
    


    public int levelState;

    void Start()
    {
        remainingSecondsToStart = 4;
        somePlayerHasReachedTheGoal = false;
        theMatchHasStarted = false;
        ControlTimeToStart();
    }

    // Update is called once per frame
    void Update()
    {
        float tickFrequency = 1f;

        if (theMatchHasStarted == false)
        {
            elapsedTimeToStart += Time.deltaTime;

            if (elapsedTimeToStart > tickFrequency)
            {

                elapsedTimeToStart = 0f;
                remainingSecondsToStart--;
                
                if (remainingSecondsToStart > 0)
                {
                    string timeLeftString = remainingSecondsToStart.ToString();
                    textFeedback.text = timeLeftString;
                }

                if (remainingSecondsToStart == 0)
                {

                    string startMessageString = "�Go Go Crazy Run!";
                    textFeedback.text = startMessageString;
                }

                if (remainingSecondsToStart < 0)
                {
                    theMatchHasStarted = true;
                    string startMessageString = "";
                    textFeedback.text = startMessageString;
                    ControlTimeToStart();
                    
                }
            }
        }
    }


    public void ControlTimeToStart()
    {
        GameObject restartButton = levelMenu.transform.Find("button_restart").gameObject;

        if (theMatchHasStarted == false)
        {
            levelMenu.SetActive(true);
            restartButton.SetActive(false);
            
        }
        else
        {
            restartButton.SetActive(true);
            levelMenu.SetActive(false);
            
        }
        
    }

    public void CheckIfSomePlayerIsAlive() {

        if (alivePlayers <= 0)
        {
            ShowResults("�Todos son perdedores!");
        }
    }

    public void ShowResults(string winner)
    {

        if (somePlayerHasReachedTheGoal == true)
        {

            finishedLevel = true;
            textFeedback.text = "Fin del nivel - El ganador es "+ winner;
            if (levelMenu.activeSelf == false)
            {
                levelMenu.SetActive(true);
            }
            //somePlayerHasReachedTheGoal = false; // puede generar conflicto si m�s de un jugador toca la meta // buscar algo m�s general.
        }

        if (alivePlayers <= 0)
        {
            finishedLevel = true;
            textFeedback.text = winner;
            if (levelMenu.activeSelf == false)
            {
                levelMenu.SetActive(true);
            }
        }


    }
}
